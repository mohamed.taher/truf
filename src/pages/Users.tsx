import { User, useUsers } from "../hooks/getUsers";
import { CustomUsersList } from "./CustomUsersList";
import { CustomInterestsList } from "./InterestsList ";

export const Users = () => {
  const users = useUsers();

  if (users.loading) return <div>loading...</div>;

  return (
    <div>
      <h1>User List</h1>
      {users.data.map((u) => (
        <div key={u.id} className="user-card">
          <p>Name: {u.name}</p>
          <CustomUsersList
            users={u.following.map(u2=>users.data.find(f=>f.id==u2) as User)}
          />
          {
            u.interests &&
            <CustomInterestsList
              ids={u.interests}
            />
          }
        </div>
      ))}
    </div>
  );
};
