import { FC } from "react";
import { Link } from "react-router-dom";

interface IList {
  data: {
    id: number;
    name: string;
  }[];
  
  item: 'String' | 'Link';
}

export const CustomList: FC<IList> = ({ data, item}) => {
  if (!data) return <div>loading...</div>;

  return (
    <div>
      {
        data.map((d) => (
          item === 'Link' ?
            (
              <div key={d.id} className="user-card">
                <Link to={`/user/${d.id}`}>{d.name}</Link>
              </div>
            ) :
            (<div key={d.id} className="user-card">{d.name}</div>)
        ))
      }
    </div>
  );
};
