import { useEffect, useState } from "react";
import { useParams } from "react-router";
import { useUsers, User } from "../hooks/getUsers";
import { CustomUsersList } from "./CustomUsersList";
import { CustomInterestsList } from "./InterestsList ";

export const UserData = () => {
  const { id } = useParams();
  const users = useUsers();
  const [user, setUser] = useState<User>();

  useEffect(()=>{
    if(users.data && id)
    setUser(users?.data.find(u=>u.id == +id))
  });

  if(!user?.id) return (<div>loading...</div> );

  return (
    <div>
      <h1>User</h1>
        <div key={user.id} className="user-card">
          <p>Name: {user.name}</p>
          <CustomUsersList
          users={users?.data?.filter((i: User)=>user.following.includes(i.id)) || []}
          />
          <CustomInterestsList
            ids={user.interests}
          />
        </div>
    </div>
  );
};
