import { FC } from "react";
import interests from "../data/interests.json";
import {CustomList} from "./List";

interface Interest {
  id: number;
  name: string;
}

export const CustomInterestsList: FC<{ ids: number[] }> = ({ ids }) => {
  return (
    <div>
      <h1>Interests List</h1>
      <CustomList
          data={interests.filter((i: Interest)=>ids.includes(i.id))}
          item='String'
          />
    </div>
  );
};
