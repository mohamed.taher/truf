import { FC } from "react";
import { User } from "../hooks/getUsers";
import { CustomList } from "./List";

export const CustomUsersList: FC<{ users: User[] }> = ({ users }) => {
  return (
    <div>
      <h1>Followers List</h1>
      <CustomList data={users} item='Link' />
    </div>
  );
};
