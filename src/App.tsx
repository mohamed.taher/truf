import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Users } from "./pages/Users";
import { UserData } from "./pages/User";
import { useEffect } from "react";

export const App = () => {
  useEffect(()=>{
    if(window.location.pathname == '/')
    window.location.href = '/users';
  }, []);

  return (
    <BrowserRouter>
      <div className="app">
        <header className="header">
          <Routes>
            <Route path="/users" element={<Users />} />
            <Route path="/user/:id" element={<UserData />} />
          </Routes>
        </header>
      </div>
    </BrowserRouter>
  );
};
